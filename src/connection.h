/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */


#ifndef CONNECTION_H
#define CONNECTION_H

typedef struct _channel channel;

typedef void (channel_success_callback)(channel 	*c);
typedef void (channel_failure_callback)(channel 	*c, 
					guint32 	 reason_code,
					const gchar 	*info,
					const gchar 	*lang);
typedef void (channel_data_callback)(channel 		*c,
				     buffer 		*data);
typedef void (channel_extended_data_callback)(channel 	*c,
					      guint32 	 data_type_code,
					      buffer 	*data);

struct _channel {
	transport *t;
	char *channel_type;
	guint32 our_channel;
	guint32 their_channel;
	guint32 recv_window_max;
	guint32 send_window;
	guint32 recv_window;
	guint32 send_packet_max;
	guint32 recv_packet_max;

	/* FIXME: use a linked list of buffers? */
	buffer *pending;

	channel_success_callback *channel_success;
	channel_failure_callback *channel_failure;
	channel_data_callback *channel_data;
	channel_extended_data_callback *channel_extended_data;
};

channel		*channel_open 		(transport	*t,
					 const gchar 	*channel_type,
					 guint32	 max_window,
					 guint32	 max_packet);

void		 channel_close 		(channel	*c);
void		 channel_write		(channel	*c,
					 const buffer	*b);
void		 channel_write_cstring	(channel 	*c,
					 const gchar 	*s);
void 		 channel_request 	(channel 	*c, 
					 const gchar 	*type, 
					 gboolean 	 want_reply, 
					 buffer 	*extra);
void 		 channel_request_simple (channel 	*c, 
					 const gchar 	*type, 
					 gboolean 	 want_reply);
void 		 channel_request_cstring (channel 	*c, 
					 const gchar 	*type, 
					 gboolean 	 want_reply,
					 const gchar 	*string);

#endif /* CONNECTION_H */
