/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include <glib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <openssl/dh.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>

#include "private.h"
#include "transport.h"
#include "utils.h"
#include "buffer.h"
#include "protocol.h"

#define PACKET_MAX_LENGTH 35000

/* FIXME: move to engines file */
static buffer *
engine_blowfish_out (transport *t, buffer *in)
{
	buffer *out;

	out = buffer_create_with_size (in->payload_length);

	BF_cbc_encrypt (in->payload, out->payload, in->payload_length,
			&t->bf_key_out, t->IV_out->payload, BF_ENCRYPT);

	out->payload_length = in->payload_length;

	return out;
}

#define BUFFER_SIZE 4096
static buffer *
engine_blowfish_in (transport *t, buffer *in)
{
	buffer *out;

	out = buffer_create_with_size (in->payload_length);

	BF_cbc_encrypt (in->payload, out->payload, in->payload_length,
			&t->bf_key_in, t->IV_in->payload, BF_DECRYPT);

	out->payload_length = in->payload_length;

	return out;
}

#define BUFFER_SIZE 4096
static void
transport_buffer_fill (transport *t)
{
	gchar temp_buffer[BUFFER_SIZE];
	gchar *new_buffer;
	int len;

	g_print ("waiting for data...\n");
	len = read(t->fd, temp_buffer, BUFFER_SIZE-1);
	if (len == 0) {
		/* FIXME */
		exit (-1);
	}
#if 0
	// <DEBUG>
	temp_buffer[len] = 0;
	g_print ("got data: `%s'\n", temp_buffer);
	// </DEBUG>
#endif
	new_buffer = g_malloc (t->internal_buffer_len+len);
	memcpy (new_buffer, t->internal_buffer, t->internal_buffer_len);
	memcpy (new_buffer+t->internal_buffer_len, temp_buffer, len);
	g_free (t->internal_buffer_base_ptr);
	t->internal_buffer = new_buffer;
	t->internal_buffer_base_ptr = new_buffer;
	t->internal_buffer_len = t->internal_buffer_len+len;
}

static guint32
transport_read_guint32 (transport *t)
{
	guint32 data;

	while (t->internal_buffer_len < 4) {
		transport_buffer_fill (t);
	}

	data = ntohl(*((guint32*)t->internal_buffer));

	t->internal_buffer_len -= 4;
	t->internal_buffer += 4;

	return data;
}

static gchar*
transport_read_bytes (transport *t, int length)
{
	gchar *bytes;

	while (t->internal_buffer_len < length) {
		transport_buffer_fill (t);
	}

	bytes = g_malloc (length);
	memcpy (bytes, t->internal_buffer, length);

	t->internal_buffer_len -= length;
	t->internal_buffer += length;

	return bytes;
}

static gchar
transport_read_byte (transport *t)
{

	while (t->internal_buffer_len < 1) {
		transport_buffer_fill (t);
	}

	t->internal_buffer_len--;
	t->internal_buffer++;

	return t->internal_buffer[-1];
}

static gchar
transport_peek_byte (transport *t)
{

	while (t->internal_buffer_len < 1) {
		transport_buffer_fill (t);
	}

	return t->internal_buffer[0];
}

#define MAX_LINE_LEN 4096
static gchar *
transport_read_line (transport *t)
{
	gchar line[MAX_LINE_LEN];
	int i;
	gchar c;

	for (i=0; i<MAX_LINE_LEN-1; i++) {
		c = transport_read_byte (t);
		if (c == '\r' && transport_peek_byte (t) == '\n') {
			transport_read_byte (t);
			break;
		}
		if ( (c == '\n') || (c == '\0')) {
			break;
		}
		line[i] = c;
	}
	line[i] = '\0';
	return g_strdup (line);
}

static void
transport_write_bytes (transport *t, gchar *bytes, int len)
{
	debug_bytes ("> ", bytes, len);
	write (t->fd, bytes, len);
}

static void
transport_write_buffer_as_bytes (transport *t, buffer *p)
{
	transport_write_bytes (t, p->payload, p->payload_length);
}

static void
transport_write_cstring (transport *t, gchar *string)
{
	transport_write_bytes (t, string, strlen (string));
}

#if 0
static void
transport_write_byte (transport *t, gchar byte)
{
	gchar b = byte;
	write (t->fd, &b, sizeof (gchar));
}

static void
transport_write_uint32 (transport *t, guint32 uint32)
{
	guint32 data = htonl (uint32);
	transport_write_bytes (t, (gchar *)&data, sizeof (data));
}
#endif

static buffer *
transport_read_block (transport *t)
{
	gint bytes_read, this_time;
	gchar *network_buffer;
	buffer *b;

	network_buffer = g_malloc0 (t->block_size);
	bytes_read = 0;
	while (bytes_read < t->block_size) {
		this_time = read (t->fd, network_buffer+bytes_read, 
				t->block_size-bytes_read);
		bytes_read += this_time;
		if (this_time == 0) {
			/* bad shit */
			/* FIXME: er - deal with it */
			exit (0);
		}
		/*g_print ("[YAK] transport_read_block read %d/%d bytes\n", bytes_read, t->block_size);*/
	}

	b = buffer_create_from_allocated_bytes (network_buffer, 
			t->block_size);
	return b;
}

static buffer *
transport_recv_packet (transport *t)
{
	buffer *p;
	guchar padding_length;
	guint32 packet_length;
	gint payload_length;
	gchar *mac;
	buffer *block_ciphertext, *block_plaintext;
	gint num_blocks;
	gint bytes_read;

	if (t->engine_in == NULL) { /* no crypto yet */
		packet_length = transport_read_guint32 (t);
		padding_length = transport_read_byte (t);
		payload_length = packet_length - padding_length - 1;
		p = buffer_create_from_allocated_bytes (
				transport_read_bytes (t, payload_length), 
				payload_length);
		debug_buffer ("< ", p);

		/* skip padding */
		g_free (transport_read_bytes (t, padding_length));
	} else {
		bytes_read = 0;
		/*g_print ("[YAK] reading packet over encrypted link...\n");*/
		/* read the first block */
		block_ciphertext = transport_read_block (t);
		bytes_read += t->block_size;
		/*g_print ("[YAK] bytes_read = %d\n", bytes_read);*/
		/*debug_buffer ("block enc", block_ciphertext);*/
		block_plaintext = t->engine_in (t, block_ciphertext);
		/*debug_buffer ("block dec", block_plaintext);*/
		packet_length = buffer_pop_uint32 (block_plaintext);
		/*g_print ("[YAK] packet_length = %d\n", packet_length);*/
		padding_length = buffer_pop_byte (block_plaintext);
		/*g_print ("[YAK] padding_length = %d\n", padding_length);*/
		payload_length = packet_length - padding_length - 1;
		/*g_print ("[YAK] payload_length = %d\n", payload_length);*/
		/* so, packet_length should be a multiple of buffer_size */
		num_blocks = (packet_length + sizeof (packet_length)) / 
					t->block_size;
		/*g_print ("[YAK] reading a total of %d blocks\n", num_blocks);*/
		num_blocks--; /* we've already read one */
		p = buffer_create ();
		buffer_append_bytes (p, block_plaintext->payload +
				(sizeof(packet_length)+sizeof(padding_length)),
				block_plaintext->payload_length -
				(sizeof(packet_length)+sizeof(padding_length)));
		buffer_free (block_plaintext);
		buffer_free (block_ciphertext);
		while (num_blocks > 0) {
			block_ciphertext = transport_read_block (t);
			bytes_read += t->block_size;
			/*g_print ("[YAK] bytes_read = %d\n", bytes_read);*/
			/*debug_buffer ("block enc", block_ciphertext);*/
			block_plaintext = t->engine_in (t, block_ciphertext);
			/*debug_buffer ("block dec", block_plaintext);*/
			buffer_append_buffer_as_bytes (p, block_plaintext);
			buffer_free (block_plaintext);
			buffer_free (block_ciphertext);
			num_blocks--;
		}
		buffer_truncate (p, payload_length);

	}

	mac = transport_read_bytes (t, t->mac_length);
	debug_bytes ("read mac", mac, t->mac_length);
	g_free (mac); /* ignore MAC right now... */
	/* FIXME: check MAC you slack bastard! */

	t->sequence_number_in++;
	t->sequence_number_in %= 0x10000000;

	return p;
}

static buffer *
transport_calculate_mac_out (transport *t, buffer *p) 
{
	buffer *mac_input, *mac_output;
	gchar md [EVP_MAX_MD_SIZE];
	gint md_len;

	/* g_print ("[YAK] calculating out MAC for sequence number %d\n", t->sequence_number_out); */

	if (t->mac_length == 0) {
		/* no MAC algorithm yet */
		return buffer_create_with_size (0);
	}

	mac_input = buffer_create ();

	buffer_append_uint32 (mac_input, t->sequence_number_out);
	buffer_append_buffer_as_bytes (mac_input, p);

	/* debug_buffer ("mac in", mac_input); */

	HMAC (t->evp_md, t->mac_key_out->payload, 
			t->mac_key_out->payload_length, mac_input->payload,
		       	mac_input->payload_length ,md, &md_len);

	buffer_free (mac_input);

	g_assert (md_len >= t->mac_length);

	mac_output = buffer_create_from_bytes (&md[0], t->mac_length);

	/* debug_buffer ("mac out", mac_output); */

	/* g_print ("[YAK] mac out = %p\n", mac_output); */

	return mac_output;
}

void
transport_send_packet (transport *t, buffer *p) 
{
	gchar padding[255];
	guchar padding_length=0;
	gint packet_length;
	buffer *mac;
	buffer *plaintext;
	buffer *ciphertext;

	g_print ("[YAK] sending packet type = %d\n", p->payload[0]);

	randomize_bytes (padding, 255);

	padding_length = t->block_size - ((p->payload_length+1) % \
				t->block_size) + 4;

	g_print ("padding_length = %d\n", padding_length);

	packet_length = p->payload_length + 1 + padding_length;

	/* build packet */
	plaintext = buffer_create ();
	buffer_append_uint32 (plaintext, packet_length);
	buffer_append_bytes (plaintext, &padding_length, 1);
	buffer_append_bytes (plaintext, p->payload, p->payload_length);
	buffer_append_bytes (plaintext, padding, padding_length);

	debug_buffer ("plaintext", plaintext);

	/* calculate MAC */
	mac = transport_calculate_mac_out (t, plaintext);

	/*
	g_print ("[YAK] mac = %p\n", mac);

	debug_buffer ("mac ", mac);
	*/

	/* encrypt the packet */
	if (t->engine_out != NULL) {
		ciphertext = t->engine_out (t, plaintext);
		buffer_free (plaintext);
	} else {
		ciphertext = plaintext;
	}

	/* debug_buffer ("ciphertext ", ciphertext); */

	t->sequence_number_out++;
	t->sequence_number_out %= 0x10000000;

	transport_write_buffer_as_bytes (t, ciphertext);
	transport_write_buffer_as_bytes (t, mac);

	buffer_free (ciphertext);
	buffer_free (mac);
}

static void
transport_send_KEXINIT (transport *t)
{
	gchar cookie[16];
	buffer *p;

	randomize_bytes (cookie, 16);

	p = buffer_create ();

	buffer_append_byte (p, SSH_MSG_KEXINIT);
	buffer_append_bytes (p, cookie, 16);

	/* send kex_algorithms */
	buffer_append_cstring_as_string (p, "diffie-hellman-group1-sha1");

	/* server_host_key_algorithms */
        buffer_append_cstring_as_string (p, "ssh-rsa");

	/* encryption_algorithms_client_to_server */
        buffer_append_cstring_as_string (p, "blowfish-cbc");

	/* encryption_algorithms_server_to_client */
        buffer_append_cstring_as_string (p, "blowfish-cbc");

	/* mac_algorithms_client_to_server */
        buffer_append_cstring_as_string (p, "hmac-sha1-96");

	/* mac_algorithms_server_to_client */
        buffer_append_cstring_as_string (p, "hmac-sha1-96");

	/* compression_algorithms_client_to_server */
        buffer_append_cstring_as_string (p, "none");

	/* compression_algorithms_server_to_client */
        buffer_append_cstring_as_string (p, "none");

	/* languages_client_to_server */
        buffer_append_cstring_as_string (p, "");

	/* languages_server_to_client */
        buffer_append_cstring_as_string (p, "");

	/* first_kex_packet_follows */
	buffer_append_boolean (p, FALSE);

	/* reserved for future extension */
	buffer_append_uint32 (p, 0);

	/* send the packet */
	transport_send_packet (t, p);

	t->I_C = p;
}

/* this function was ripped from OpenSSH */
static gboolean
dh_pub_is_valid (DH *dh, BIGNUM *dh_pub)
{
        int i;
        int n = BN_num_bits(dh_pub);
        int bits_set = 0;

        if (dh_pub->neg) {
                g_print ("invalid public DH value: negativ");
                return FALSE;
        }
        for (i = 0; i <= n; i++) {
                if (BN_is_bit_set (dh_pub, i)) {
                        bits_set++;
		}
	}

        g_print ("bits set: %d/%d\n", bits_set, BN_num_bits(dh->p));

        /* if g==2 and bits_set==1 then computing log_g(dh_pub) is trivial */
        if (bits_set > 1 && (BN_cmp(dh_pub, dh->p) == -1)) {
                return TRUE;
	}

        g_print ("invalid public DH value (%d/%d)", bits_set, BN_num_bits(dh->p));
        return 0;
}

static void
transport_send_KEXDH_INIT (transport *t)
{
	buffer *p;

	gchar *group1_hex = 
	  "FFFFFFFF" "FFFFFFFF" "C90FDAA2" "2168C234" "C4C6628B" "80DC1CD1"
	  "29024E08" "8A67CC74" "020BBEA6" "3B139B22" "514A0879" "8E3404DD"
	  "EF9519B3" "CD3A431B" "302B0A6D" "F25F1437" "4FE1356D" "6D51C245"
	  "E485B576" "625E7EC6" "F44C42E9" "A637ED6B" "0BFF5CB6" "F406B7ED"
	  "EE386BFB" "5A899FA5" "AE9F2411" "7C4B1FE6" "49286651" "ECE65381"
	  "FFFFFFFF" "FFFFFFFF";
	gchar *gen = "2";

	g_print ("transport_send_KEXDH_INIT\n");

	t->dh = DH_new ();
	g_assert (t->dh != NULL);

	/* FIXME: check these return vals */
        BN_hex2bn(&t->dh->p, group1_hex);
	BN_hex2bn(&t->dh->g, gen);

        do {
                t->dh->priv_key = BN_new();
		g_assert (t->dh->priv_key != NULL);

                BN_rand(t->dh->priv_key, 1024, 0, 0);

                DH_generate_key(t->dh);

        } while (!dh_pub_is_valid(t->dh, t->dh->pub_key));
	
	p = buffer_create ();

	t->e = t->dh->pub_key;

	buffer_append_byte (p, SSH_MSG_KEXDH_INIT);
	buffer_append_bignum (p, t->e);
	transport_send_packet (t, p);

	/* FIXME: save or free p */
}

static void
transport_send_NEWKEYS (transport *t)
{
	buffer *p;

	p = buffer_create ();
	buffer_append_byte (p, SSH_MSG_NEWKEYS);
	transport_send_packet (t, p);
	buffer_free(p);
}

static void
transport_recv_KEXDH_REPLY (transport *t, buffer *p)
{
	buffer *K_S;
	buffer *sig_H;
	char *K_buf;
	int K_buf_len;
	buffer *H_in;

	g_print ("transport_recv_KEXDH_REPLY\n");

	K_S = buffer_pop_string_as_buffer (p);

	debug_buffer ("K_S ", K_S);

	t->f = buffer_pop_bignum (p);
        fprintf(stdout, "f: ");
        BN_print_fp(stdout, t->f);
        fprintf(stdout, "\n");
	
	sig_H = buffer_pop_string_as_buffer (p);

	debug_buffer ("sig_H ", sig_H);

        K_buf = g_malloc0 (DH_size (t->dh));
        K_buf_len = DH_compute_key(K_buf, t->f, t->dh);
	debug_bytes ("K_buf ", K_buf, K_buf_len);

	t->K = BN_bin2bn(K_buf, K_buf_len, NULL);

	/* calulate:
	 * H = hash (V_C || V_S || I_C || I_S || K_S || e || f || K)
	 */

	H_in = buffer_create ();
	buffer_append_cstring_as_string (H_in, t->V_C);
	buffer_append_cstring_as_string (H_in, t->V_S);
	buffer_append_buffer_as_string (H_in, t->I_C);
	buffer_append_buffer_as_string (H_in, t->I_S);
	buffer_append_buffer_as_string (H_in, K_S);
	buffer_append_bignum (H_in, t->e);
	buffer_append_bignum (H_in, t->f);
	buffer_append_bignum (H_in, t->K);

	debug_buffer ("H_in ", H_in);

	t->H = buffer_hash (H_in);

	debug_buffer ("H ", t->H);

	/* fixme: check server host key signature thingo */

	/* .... */
	/* FIXME: free this shit */

	transport_send_NEWKEYS (t);
}

static buffer *
transport_compute_key (transport *t, gchar id, gint length)
{
	buffer *b;
	buffer *hash;

	/* build input to hash */
	b = buffer_create ();
	buffer_append_bignum (b, t->K);
	buffer_append_buffer_as_bytes (b, t->H);
	buffer_append_byte (b, id);
	buffer_append_buffer_as_bytes (b, t->H);

	/* hash it */
	hash = buffer_hash (b);

	/* free input buffer */
	buffer_free (b);

	/* truncate hash to desired length */
	g_assert (hash->payload_length >= length); /* make sure theres enough */
	hash->payload_length = length;

	return hash;
}

static void
transport_recv_NEWKEYS (transport *t, buffer *p)
{
	buffer *key_out;
	buffer *key_in;

	buffer_free (p);

	g_print ("recieved NEWKEYS\n");

	/* work out hashes */
	t->IV_out = transport_compute_key (t, 'A', 8);
	debug_buffer ("IV_out ", t->IV_out);

	t->IV_in = transport_compute_key (t, 'B', 8);
	debug_buffer ("IV_in ", t->IV_in);

	key_out = transport_compute_key (t, 'C', 16);
	debug_buffer ("key_out ", key_out);

	key_in = transport_compute_key (t, 'D', 16);
	debug_buffer ("key_in ", key_in);

	t->mac_key_out = transport_compute_key (t, 'E', 20);
	debug_buffer ("mac_key_out ", t->mac_key_out);

	t->mac_key_in = transport_compute_key (t, 'F', 20);
	debug_buffer ("mac_key_in ", t->mac_key_in);

	/* FIXME: support other crypto */

	/* setup blowfish engines */
	BF_set_key (&t->bf_key_out, key_out->payload_length, key_out->payload);
	BF_set_key (&t->bf_key_in, key_in->payload_length, key_in->payload);
	t->engine_out = engine_blowfish_out;
	t->engine_in = engine_blowfish_in;

	t->evp_md = EVP_sha1 ();
	t->mac_length = 12;

	if (t->transport_connected_callback != NULL) {
		t->transport_connected_callback (t);
	}
}


void
transport_send_SERVICE_REQUEST (transport *t, gchar *service)
{
	buffer *p;

	p = buffer_create();
	buffer_append_byte (p, SSH_MSG_SERVICE_REQUEST);
	buffer_append_cstring_as_string (p, service);
	transport_send_packet (t, p);
	buffer_free(p);
}

#if 0
void
transport_auth_password (transport *t, gchar *username, gchar *password)
{
	transport_send_SERVICE_REQUEST (t, SERVICE_USERAUTH);
	transport_send_USERAUTH_REQUEST_password (t, username, password);
}
#endif

static void 
transport_recv_KEXINIT (transport *t, buffer *p)
{
	g_print ("[YAK] transport_recv_KEXINIT\n");

	/* read the server's KEXINIT */
	t->I_S = p;

	/* FIXME: check that they're going to send us what we're asking for */

	/* lets do diffie-hellman-group1-sha1 */
	transport_send_KEXDH_INIT (t);
}

static void 
transport_recv_SERVICE_ACCEPT (transport *t, buffer *p)
{
	char *service_name;

	service_name = buffer_pop_string_as_cstring (p);

	g_print ("[YAK] service accepted: `%s'\n", service_name);
}

void
transport_handle_packet (transport *t)
{
	guchar				 type;
        buffer				*p;
	GList				*iter;
	transport_packet_handler 	*handler;
	gboolean			 handled;
	
	p = transport_recv_packet (t);

	handled = FALSE;
	iter = t->private->packet_handlers;
	while (iter && !handled) {
		handler = iter->data;
		if (handler) {
			buffer_seek (p, 0);
			handled = handler (t, p);
		}
		iter = iter->next;
	}

	if (handled) {
		return;
	}

	buffer_seek (p, 0);
	type = buffer_pop_byte (p);
	switch (type) {
		case SSH_MSG_UNIMPLEMENTED:
			g_print ("UNIMPLEMENTED for packet %d\n", 
					buffer_pop_uint32 (p));
			break;

		case SSH_MSG_IGNORE:
			g_print ("[YAK] ignoring packet\n");
			debug_buffer ("ignoring", p);
			break;

		case SSH_MSG_KEXINIT:
			transport_recv_KEXINIT (t, p);
			break;

		case SSH_MSG_KEXDH_REPLY:
			transport_recv_KEXDH_REPLY (t, p);
			break;

		case SSH_MSG_NEWKEYS:
			transport_recv_NEWKEYS (t, p);
			break;

		case SSH_MSG_SERVICE_ACCEPT:
			transport_recv_SERVICE_ACCEPT (t, p);
			break;

		default:
			g_print ("[YAK] unknown packet type %d\n", type);
			break;
	}
}

transport *
transport_create ()
{
	transport *t = g_new0 (transport, 1);
	t->private = g_new0 (transport_private, 1);

	return t;
}

gboolean
transport_connect (transport *t, gchar *hostname, gint port) 
{
	struct hostent *he;
	struct sockaddr_in sin;

	t->block_size = 8;

	/* create the socket */
	t->fd = socket (PF_INET, SOCK_STREAM, 0);
	if (t->fd == -1) {
		g_free (t);
		return FALSE;
	}

	/* look up the host name */
	he = gethostbyname (hostname);
	if (he == NULL) {
		g_free (t);
		return FALSE;
	}

	/* connect to the server */
        sin.sin_family = he->h_addrtype;
        sin.sin_addr = * (struct in_addr *) he->h_addr;
        sin.sin_port = htons (port);
	if (connect (t->fd, (struct sockaddr *)&sin, sizeof (sin)) == -1) {
		g_free (t);
		return FALSE;
	}

	/* send our version info */
	t->V_C = g_strdup ("SSH-2.0-libssh_0.0");
	transport_write_cstring (t, t->V_C);
	transport_write_cstring (t, "\r\n");

	/* read their version info */
	for (;;) {
		t->V_S = transport_read_line (t);
		if (!strncmp ("SSH-", t->V_S, 4)) {
			break;
		}
	}

	/* send our KEXINIT */
	transport_send_KEXINIT (t);

	return TRUE;
}

void
transport_add_packet_handler (transport *t, transport_packet_handler *h)
{
	t->private->packet_handlers = 
		g_list_append (t->private->packet_handlers, h);
}

void
transport_remove_packet_handler (transport *t, transport_packet_handler *h)
{
	t->private->packet_handlers = 
		g_list_remove (t->private->packet_handlers, h);
}

