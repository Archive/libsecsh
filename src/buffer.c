/* part of libssh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include <glib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <openssl/dh.h>
#include <openssl/sha.h>

#include "transport.h"
#include "utils.h"
#include "buffer.h"

#define PACKET_MAX_LENGTH 35000

void
buffer_free (buffer *p)
{
	return; /* FIXME oh :( */
	g_free (p->payload);
	g_free (p);
}

buffer *
buffer_create_from_allocated_bytes (gchar *bytes, gint length) {
	buffer *p;

	p = g_new0 (buffer, 1);
	p->payload_length = length;
	p->payload = bytes;
	p->buffer_size = length;

	return p;
}

buffer *
buffer_create_from_bytes (const gchar *bytes, gint length) 
{
	gchar *allocated;

	allocated = g_malloc0 (length);
	memcpy (allocated, bytes, length);

	return buffer_create_from_allocated_bytes (allocated, length);
}

buffer *
buffer_copy (const buffer *b) 
{
	buffer *p;

	p = buffer_create_from_bytes (b->payload, b->payload_length);
	p->index = b->index;
	/* we're not copying the buffer_length because there isn't spare space
	 * in the new buffer. So its not a true exact copy - just a copy of
	 * the data.
	 */

	return p;
}

buffer *
buffer_create_with_size (gint size)
{
	buffer *p;

	p = buffer_create_from_allocated_bytes (g_malloc0 (size), size);
	p->payload_length = 0;

	return p;
}

buffer *
buffer_create ()
{
	return buffer_create_with_size (PACKET_MAX_LENGTH);
}

void
buffer_append_byte (buffer *p, gchar byte) {
	g_assert (p->buffer_size - p->payload_length > 1);

	p->payload[p->payload_length] = byte;
	p->payload_length++;
}

void
buffer_append_bytes (buffer *p, const gchar *bytes, gint length) {
	g_assert (p->buffer_size - p->payload_length > length);

	memcpy (p->payload+p->payload_length, bytes, length);
	p->payload_length += length;
}

void
buffer_append_uint32 (buffer *p, guint32 uint32)
{
	guint32 data = htonl (uint32);
	buffer_append_bytes (p, (gchar *)&data, sizeof (data));
}

void
buffer_append_string (buffer *p, const gchar *string, gint length)
{
	buffer_append_uint32 (p, length);
	buffer_append_bytes (p, string, length);
}

void
buffer_append_buffer_as_string (buffer *p, buffer *b)
{
	/*buffer_append_string (p, b->payload+b->index, b->payload_length);*/
	buffer_append_string (p, b->payload, b->payload_length);
}

void
buffer_append_buffer_as_bytes (buffer *p, buffer *b)
{
	/*buffer_append_bytes (p, b->payload+b->index, b->payload_length);*/
	buffer_append_bytes (p, b->payload, b->payload_length);
}

void
buffer_append_cstring_as_string (buffer *p, const gchar *string) 
{
	buffer_append_string (p, string, strlen (string));
}

void
buffer_append_boolean (buffer *p, gboolean boolean) {
	if (boolean) {
		buffer_append_byte (p, 1);
	} else {
		buffer_append_byte (p, 0);
	}
}

void
buffer_append_bignum (buffer *p, BIGNUM *value)
{
	int length = BN_num_bytes (value) + 1;
	guchar *string = g_malloc0 (length);
	int actual_length, hasnohigh;
	int i, carry;
	guchar *uc;

	actual_length = BN_bn2bin (value, string+1);

	g_assert (actual_length == length-1);

	hasnohigh = (string[1] & 0x80) ? 0 : 1;

	if (value->neg) {
		uc = string;
		for (i = length-1, carry = 1; i >= 0; i--) {
			uc[i] ^= 0xFF;
			if (carry) {
				carry = !++uc[i];
			}
		}
		
	}
	buffer_append_string (p, string+hasnohigh, length-hasnohigh);

}

guchar
buffer_pop_byte (buffer *p) {
	guchar byte;

	g_assert (p->payload_length-p->index >= 1);

	byte = p->payload[p->index];
	
	p->index++;

	return byte;
}

guint32
buffer_pop_uint32 (buffer *p) {
	guint32 uint32;

	g_assert (p->payload_length-p->index >= 4);

	uint32 = *((guint32 *)(p->payload+p->index));
	uint32 = ntohl(uint32);

	p->index += 4;

	return uint32;
}

guchar *
buffer_pop_string (buffer *p, gint *length)
{
	guchar *bytes;

	*length = buffer_pop_uint32 (p);

	g_assert (p->payload_length-p->index >= *length);

	bytes = g_malloc0 ((*length)+1); /* ensure bytes is null terminated */
	memcpy (bytes, p->payload+p->index, *length);

	p->index += *length;

	return bytes;
}

buffer *
buffer_pop_string_as_buffer (buffer *p)
{
	buffer *b;

	b = buffer_create ();
	b->payload = buffer_pop_string (p, &b->payload_length);

	return b;
}

guchar *
buffer_pop_string_as_cstring (buffer *p)
{
	gint length; /* we'll discard */
	
	/* buffer_pop_string already nul-terminates the string */
	return buffer_pop_string (p, &length);
}

BIGNUM *
buffer_pop_bignum (buffer *p)
{
	guchar *bytes;
	gint length;
	BIGNUM *bn;

	bytes = buffer_pop_string (p, &length);

	bn = BN_new ();

	BN_bin2bn (bytes, length, bn);

	g_free (bytes);

	return bn;
}

buffer *
buffer_hash (buffer *p)
{
	buffer *b;
	guchar digest[EVP_MAX_MD_SIZE];
	EVP_MD *evp_md = EVP_sha1();
	EVP_MD_CTX md;

	EVP_DigestInit(&md, evp_md);
	EVP_DigestUpdate(&md, p->payload, p->payload_length);
	EVP_DigestFinal(&md, digest, NULL);
	b = buffer_create_from_bytes (digest, evp_md->md_size);

	return b;
}

void
buffer_truncate (buffer *b, gint length)
{
	g_assert (length <= b->payload_length);
	/* FIXME: actually free some shit? */
	b->payload_length = length;
}

void
buffer_seek (buffer *b, guint index)
{
	g_assert (index < b->payload_length);
	b->index = index;
}

buffer *
buffer_concat (const buffer *a, const buffer *b)
{
	buffer *q;

	g_assert (a != NULL);
	g_assert (b != NULL);
	
	q = buffer_create_with_size (a->payload_length + b->payload_length);

	memcpy (q->payload, a->payload, a->payload_length);
	memcpy (q->payload+a->payload_length, b->payload, b->payload_length);

	return q;
}
