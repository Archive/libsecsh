/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef USERAUTH_H
#define USERAUTH_H

#include "transport.h"

typedef void (userauth_success_callback)(transport *t, gboolean successful);
typedef void (userauth_banner_callback)(transport *t, const gchar *message, const gchar *language);
typedef const gchar *(userauth_password_callback)(transport *t, const gchar *username);

/* FIXME: support public key crypto (required by spec) */
/* FIXME: support PASSWD_CHANGEREQ */
/* FIXME: support host based authentication */

void userauth_start (transport *t, gchar *username, 
		userauth_success_callback *success_cb,
		userauth_banner_callback *banner_cb,
		userauth_password_callback *password_cb);

#endif /* USERAUTH_H */
