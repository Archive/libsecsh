 /* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef TRANSPORT_H
#define TRANSPORT_H

#include <openssl/dh.h>
#include <openssl/blowfish.h>
#include <openssl/evp.h>

#include "buffer.h"

typedef struct _transport transport;

typedef buffer *(engine)(transport *t, buffer *in);
typedef void (transport_callback)(transport *t);
typedef gboolean (transport_packet_handler)(transport *t, buffer *p);

typedef struct _transport_private transport_private;

struct _transport {
	int fd;

	void *user_data;

	guint32 sequence_number_out;
	guint32 sequence_number_in;

	EVP_MD *evp_md; /* MAC algorithm thingo */
	int mac_length;
	buffer *mac_key_out;
	buffer *mac_key_in;

	int block_size;

	gchar *V_C;
	gchar *V_S;

	char *internal_buffer;
	int internal_buffer_len;
	char *internal_buffer_base_ptr;

	buffer *I_C;
	buffer *I_S;

	BIGNUM *K;
	BIGNUM *e;
	BIGNUM *f;

	buffer *H;

	DH *dh;

	buffer *IV_out;
	buffer *IV_in;
	BF_KEY bf_key_out;
	BF_KEY bf_key_in;

	engine *engine_out;
	engine *engine_in;

	/* callbacks */
	transport_callback *transport_connected_callback;
	transport_callback *transport_authenticated_callback;

	transport_private *private;
};

transport 	*transport_create 		();
gboolean       	 transport_connect	 	(transport 	*t,
					 	 gchar 		*hostname, 
					 	 gint 		 port);
void		 transport_handle_packet 	(transport 	*t);
#if 0
void		 transport_auth_password 	(transport 	*t,
						 gchar		*username, 
						 gchar		*password);
#endif
void		 transport_add_packet_handler (transport	*t,
						 transport_packet_handler *h);
void		 transport_remove_packet_handler (transport	*t,
						 transport_packet_handler *h);

#endif /* TRANSPORT_H */
