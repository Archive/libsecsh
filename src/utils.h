/* part of libssh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef UTILS_H
#define UTILS_H

#include <glib.h>

#include "buffer.h"

void debug_bytes (gchar *prefix, gchar *data, gint length);
void debug_buffer (gchar *prefix, buffer *buffer);
void randomize_bytes (gchar *buffer, gint length);


#endif /* UTILS_H */
