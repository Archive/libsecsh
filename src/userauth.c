/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include "userauth.h"
#include "buffer.h"
#include "private.h"
#include "protocol.h"

static void
userauth_auth_password (transport *t, const gchar *username, 
		const gchar *password)
{
	buffer *p;

	p = buffer_create();
	buffer_append_byte (p, SSH_MSG_USERAUTH_REQUEST);
	buffer_append_cstring_as_string (p, username);
	buffer_append_cstring_as_string (p, "ssh-connection");
	buffer_append_cstring_as_string (p, "password");
	buffer_append_boolean (p, FALSE);
	buffer_append_cstring_as_string (p, password);
	transport_send_packet (t, p);
	buffer_free(p);
}

/* user authentication has failed. report failure and clean up */
static void
userauth_failed (transport *t) {
	userauth *u = t->private->userauth;

	t->private->userauth = NULL;

	if (u->success_callback) {
		u->success_callback (t, FALSE);
	}

	g_free (u);
}

/* move to the next step of user authentication */
static void
userauth_next_step (transport *t)
{
	userauth *u = t->private->userauth;
	const gchar *password;

	u->state++;

	switch (u->state) {
		case STATE_PASSWORD:
			if (u->password_callback) {
				password = u->password_callback (t, 
						u->username);
				if (password == NULL) {
					/* user has cancelled auth */
					userauth_failed (t);
					break;
				}
				userauth_auth_password (t, u->username, 
						password);
			} else {
				userauth_next_step (t);
			}
			break;

		case STATE_FAILED:
		default:
			userauth_failed (t);
			break;
	}
}

static gboolean
userauth_recv_SERVICE_ACCEPT (transport *t, buffer *p)
{
	char *service_name;

	service_name = buffer_pop_string_as_cstring (p);

	g_print ("[YAK] service accepted: `%s'\n", service_name);

	if (!strcmp (service_name, SERVICE_USERAUTH)) {
		userauth_next_step (t);
	} else {
		return FALSE;
	}
	return TRUE;
}

static gboolean
userauth_packet_handler (transport *t, buffer *p)
{
	guchar type;

	buffer_seek (p, 0);
	type = buffer_pop_byte (p);

	switch (type) {
		case SSH_MSG_USERAUTH_SUCCESS:
			g_print ("[YAK] userauth success!\n");

			/* inform the app */
			if (t->transport_authenticated_callback) {
				t->transport_authenticated_callback (t);
			}

			/* set up a handler for connection/channel packets */
			transport_add_packet_handler (t, 
					connection_packet_handler);
			return TRUE;
			break;

		case SSH_MSG_SERVICE_ACCEPT:
			return userauth_recv_SERVICE_ACCEPT (t, p);
			break;


	}

	return FALSE;
}

void 
userauth_start (transport *t, gchar *username, 
		userauth_success_callback *success_cb,
		userauth_banner_callback *banner_cb,
		userauth_password_callback *password_cb) 
{
	userauth *u;

	u = g_new0 (userauth, 1);

	u->username = username;
	u->success_callback = success_cb;
	u->banner_callback = banner_cb;
	u->password_callback = password_cb;
	
	t->private->userauth = u;

	transport_add_packet_handler (t, userauth_packet_handler);

	transport_send_SERVICE_REQUEST (t, SERVICE_USERAUTH);
}


