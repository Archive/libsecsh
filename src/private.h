/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef PRIVATE_H
#define PRIVATE_H

#include "transport.h"
#include "userauth.h"
#include "connection.h"

typedef struct _userauth {
	char 				*username;
	userauth_success_callback 	*success_callback;
        userauth_banner_callback 	*banner_callback;
        userauth_password_callback 	*password_callback;
	enum {
		STATE_INITIAL = 0,
		STATE_PASSWORD,
		STATE_FAILED
	} state;
} userauth;

struct _transport_private {
	/* GList of transport_packet_handler */
	GList 				*packet_handlers;
	userauth		 	*userauth;

	/* connection / channel stuff */
	GList 				*channels;
};

/* private APIs */
void	transport_send_packet 		(transport 	*t,
					 buffer 	*p);
void	transport_send_SERVICE_REQUEST	(transport 	*t, 
					 gchar 		*service);
gboolean connection_packet_handler 	(transport 	*t, 
					 buffer *p);
  

#endif /* PRIVATE_H */
