/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include "transport.h"
#include "connection.h"
#include "private.h"
#include "protocol.h"
#include "utils.h"

static channel *
channel_from_buffer (transport *t, buffer *p)
{
	guint32 our_channel_number;
	channel *c;

	our_channel_number = buffer_pop_uint32 (p);
	/* FIXME: lock next line (same lock as used in channel_open)*/
	c = (channel *)g_list_nth_data (t->private->channels, 
			our_channel_number);

	return c;
}

static void
channel_OPEN_CONFIRMATION (transport *t, buffer *p)
{
	channel *c;

	c = channel_from_buffer (t, p);
	if (c == NULL) {
		/* bugger... */
		return;
	}

	c->their_channel = buffer_pop_uint32 (p);
	c->send_window = buffer_pop_uint32 (p);
	c->send_packet_max = buffer_pop_uint32 (p);

	if (c->channel_success != NULL) {
		c->channel_success (c);
	}
}

static void
channel_DATA (transport *t, buffer *p)
{
	channel *c;
	buffer *data;

	c = channel_from_buffer (t, p);

	data = buffer_pop_string_as_buffer (p);

	debug_buffer ("data", data);
}

static void
channel_EXTENDED_DATA (transport *t, buffer *p)
{
	channel 	*c;
	guint32 	 type;
	buffer 		*data;

	c = channel_from_buffer (t, p);

	type = buffer_pop_uint32 (p);
	data = buffer_pop_string_as_buffer (p);

	g_print ("[YAK] extended data type = %d\n", type);
	debug_buffer ("ext data", data);
}

static void
channel_WINDOW_ADJUST (transport *t, buffer *p)
{
	channel		*c;
	guint32		 bytes;

	c = channel_from_buffer (t, p);
	bytes = buffer_pop_uint32 (p);

	c->send_window += bytes;
}

gboolean
connection_packet_handler (transport *t, buffer *p)
{
	guchar type;
	/* connection *c = t->private->connection; */

	buffer_seek (p, 0);
	type = buffer_pop_byte (p);

	switch (type) {
		case SSH_MSG_CHANNEL_OPEN_CONFIRMATION:
			g_print ("[YAK] connection success!\n");
			channel_OPEN_CONFIRMATION (t, p);
			return TRUE;
			break;

		case SSH_MSG_CHANNEL_OPEN_FAILURE:
			g_print ("[YAK] connection failure :-( \n");
			return TRUE;
			break;

		case SSH_MSG_CHANNEL_DATA:
			channel_DATA (t, p);
			return TRUE;
			break;

		case SSH_MSG_CHANNEL_EXTENDED_DATA:
			channel_EXTENDED_DATA (t, p);
			return TRUE;
			break;
		case SSH_MSG_CHANNEL_WINDOW_ADJUST:
			channel_WINDOW_ADJUST (t, p);
	}

	return FALSE;
}


channel *
channel_open (transport *t, const gchar *channel_type, guint32 max_window, 
		guint32 max_packet)
{
	buffer *p;
	channel *c;

	c = g_new0 (channel, 1);
	c->t = t;

	/* FIXME: make me threadsafe: */
	/* exclusive block starts */
	c->our_channel = g_list_length (t->private->channels);
	t->private->channels = g_list_append (t->private->channels, c);
	/* exclusive block ends */

	c->channel_type = g_strdup (channel_type);
	c->recv_window_max = max_window;
	c->recv_window = c->recv_window_max;
	c->recv_packet_max = max_packet;
	c->recv_window = max_window;
	c->send_window = -1; /* we don't know this yet */
	c->send_packet_max = -1; /* we don't know this yet */

	p = buffer_create();
	buffer_append_byte (p, SSH_MSG_CHANNEL_OPEN);
	buffer_append_cstring_as_string (p, channel_type);
	buffer_append_uint32 (p, c->our_channel);
	buffer_append_uint32 (p, c->recv_window);
	buffer_append_uint32 (p, c->recv_packet_max);
	transport_send_packet (t, p);
	buffer_free(p);

	return c;
}

static void
channel_actually_write_buffer (channel *c, buffer *b)
{
	buffer *p = buffer_create_with_size (b->payload_length + 32);

	buffer_append_byte (p, SSH_MSG_CHANNEL_DATA);
	buffer_append_uint32 (p, c->their_channel);
	buffer_append_buffer_as_string (p, b);
	transport_send_packet (c->t, p);
	buffer_free (p);
}

#define MIN3(x,y,z) MIN(MIN((x),(y)),(z))

static void
channel_try_to_write_pending (channel *c)
{
	gint packet_length;
	buffer *new_pending;
	buffer *p;

	g_print ("[YAK] channel_try_to_write_pending\n");

	if (c->pending == NULL) {
		/* I guess theres nothing to write */
		g_print ("[YAK] nothing to write...\n");
		return;
	}

	g_print ("[YAK] send_window = %d\n", c->send_window);
	g_print ("[YAK] send_packet_max = %d\n", c->send_packet_max);
	g_print ("[YAK] pending->payload_length = %d\n", c->pending->payload_length);

	while (c->send_window > 0) {
		packet_length = MIN3(c->send_window, c->send_packet_max, 
				c->pending->payload_length);
		if (packet_length <= 0) {
			break;
		}
		g_print ("[YAK] writing %d bytes from the pending buffer\n", packet_length);
		p = buffer_create_from_bytes (c->pending->payload,
				packet_length);
		new_pending = buffer_create_from_bytes 
			(c->pending->payload + packet_length, 
			 c->pending->payload_length - packet_length);
		buffer_free (c->pending);
		c->pending = new_pending;
		c->send_window -= packet_length;
		channel_actually_write_buffer (c, p);
		buffer_free (p);
	}
}

void
channel_write (channel *c, const buffer *b)
{
	buffer *new_pending;

	if (c->pending == NULL) {
		c->pending = buffer_copy (b);
	} else {
		new_pending = buffer_concat (c->pending, b);
		buffer_free (c->pending);
		c->pending = new_pending;
	}


	channel_try_to_write_pending (c);
}

void
channel_write_cstring (channel *c, const gchar *s)
{
	buffer *tmp;

	tmp = buffer_create_from_bytes (s, strlen (s));

	channel_write (c, tmp);

	buffer_free (tmp);
}

void
channel_request (channel *c, const gchar *type, gboolean want_reply, 
		buffer *extra)
{
	buffer *p = buffer_create ();

	buffer_append_byte (p, SSH_MSG_CHANNEL_REQUEST);
	buffer_append_uint32 (p, c->their_channel);
	buffer_append_cstring_as_string (p, type);
	buffer_append_boolean (p, want_reply);
	buffer_append_buffer_as_bytes (p, extra);
	transport_send_packet (c->t, p);
	buffer_free (p);
}

void
channel_request_simple (channel *c, const gchar *type, gboolean want_reply)
{
	buffer *tmp = buffer_create_with_size (0);

	channel_request (c, type, want_reply, tmp);

	buffer_free (tmp);
}

void
channel_request_cstring (channel *c, const gchar *type, gboolean want_reply,
		const gchar *string)
{
	buffer *tmp = buffer_create (0);

	buffer_append_cstring_as_string (tmp, string);

	channel_request (c, type, want_reply, tmp);

	buffer_free (tmp);
}
