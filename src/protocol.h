/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef PROTOCOL_H
#define PROTOCOL_H

/* message ids */
#define SSH_MSG_DISCONNECT			1
#define SSH_MSG_IGNORE				2
#define SSH_MSG_UNIMPLEMENTED			3
#define SSH_MSG_DEBUG				4
#define SSH_MSG_SERVICE_REQUEST 		5
#define SSH_MSG_SERVICE_ACCEPT 			6

#define SSH_MSG_KEXINIT 			20
#define SSH_MSG_NEWKEYS 			21

#define SSH_MSG_KEXDH_INIT 			30
#define SSH_MSG_KEXDH_REPLY 			31

#define SSH_MSG_USERAUTH_REQUEST 		50
#define SSH_MSG_USERAUTH_FAILURE 		51
#define SSH_MSG_USERAUTH_SUCCESS 		52
#define SSH_MSG_USERAUTH_BANNER 		53

#define SSH_MSG_GLOBAL_REQUEST			80
#define SSH_MSG_REQUEST_SUCCESS			81
#define SSH_MSG_REQUEST_FAILURE			82

#define SSH_MSG_CHANNEL_OPEN			90
#define SSH_MSG_CHANNEL_OPEN_CONFIRMATION	91
#define SSH_MSG_CHANNEL_OPEN_FAILURE		92
#define SSH_MSG_CHANNEL_WINDOW_ADJUST		93
#define SSH_MSG_CHANNEL_DATA			94
#define SSH_MSG_CHANNEL_EXTENDED_DATA		95
#define SSH_MSG_CHANNEL_EOF			96
#define SSH_MSG_CHANNEL_CLOSE			97
#define SSH_MSG_CHANNEL_REQUEST			98
#define SSH_MSG_CHANNEL_SUCCESS			99
#define SSH_MSG_CHANNEL_FAILURE			100


/* connection open failue message ids */
#define SSH_OPEN_ADMINISTRATIVELY_PROHIBITED	1
#define SSH_OPEN_CONNECT_FAILED			2
#define SSH_OPEN_UNKNOWN_CHANNEL_TYPE		3
#define SSH_OPEN_RESOURCE_SHORTAGE		4

/* extended connection data types */
#define SSH_EXTENDED_DATA_STDERR		1

/* service ids */
#define SERVICE_USERAUTH 	"ssh-userauth"
#define SERVICE_CONNECTION 	"ssh-connection"

#endif /* PROTOCOL_H */
