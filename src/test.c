/* part of libssh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include <glib.h>
#include <unistd.h>

#include "transport.h"
#include "userauth.h"
#include "connection.h"

static const gchar *
get_password (transport *t, const gchar *username) 
{
	return "testpass";
}

static void
auth_success (transport *t, gboolean succeeded)
{
	g_print ("AUTH SUCCESS (%d)\n", (gint)succeeded);
}

static void
banner (transport *t, const gchar *message, const gchar *lang)
{
	g_print ("BANNER (\"%s\", \"%s\")\n", message, lang);
}

static void
transport_connected (transport *t) {
	g_print ("TRANSPORT CONNECTED!\n");
	userauth_start (t, "test", auth_success, banner, get_password);
}

static void
session_success (channel *c)
{
	g_print ("SESSION CHANNEL OPENED SUCCESSFULLY\n");

	/*channel_request_cstring (c, "exec", FALSE, "touch /tmp/you-suck");*/
	channel_request_simple (c, "shell", FALSE);
	channel_write_cstring (c, "echo fuck>/tmp/bozo\n");
}

static void
transport_authenticated (transport *t) 
{
	channel *c;
	g_print ("TRANSPORT AUTHENTICATED!\n");
	/* okay, lets open a channel */

	c = channel_open (t, "session", 256, 256);
	c->channel_success = session_success;
}

int 
main (int argc, char **argv) {
	transport *t;
	int port = 24;
	gboolean result;
	char *host = "localhost";

	if (argc == 2) {
		host = argv[1];
	}

	t = transport_create ();
	t->transport_connected_callback = transport_connected;
	t->transport_authenticated_callback = transport_authenticated;
	result = transport_connect (t, host, port);

	if (!result) {
		g_print ("couldn't connect\n");
		return -1;
	}

	g_print ("remote protocol is `%s'\n", t->V_S);

	for (;;) {
		transport_handle_packet (t); /* read packet from the network */
	}

	sleep (30);

	return 0;
}
