/* part of libsecsh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <openssl/bn.h>
#include <glib.h>

struct _buffer {
	gchar *payload;
	gint payload_length;
	gint index; /* when we're parsing the buffer - how far in are we? */
	gint buffer_size; /* size of allocated area */
};

typedef struct _buffer buffer;

buffer 	*buffer_create			();
buffer  *buffer_create_from_allocated_bytes (gchar *bytes, gint length);
buffer	*buffer_create_from_bytes 	(const gchar *bytes, gint length);
buffer	*buffer_create_with_size 	(gint size);
void 	 buffer_free			(buffer *p);

void 	 buffer_append_byte		(buffer *p, gchar byte);
void 	 buffer_append_bytes		(buffer *p, const gchar *bytes, gint length);
void 	 buffer_append_uint32		(buffer *p, guint32 uint32);
void 	 buffer_append_string		(buffer *p, const gchar *string, gint length);
void 	 buffer_append_buffer_as_string	(buffer *p, buffer *b);
void	 buffer_append_buffer_as_bytes 	(buffer *p, buffer *b);
void 	 buffer_append_buffer_as_bytes	(buffer *p, buffer *b);
void 	 buffer_append_cstring_as_string(buffer *p, const gchar *string);
void 	 buffer_append_boolean		(buffer *p, gboolean boolean);
void 	 buffer_append_bignum		(buffer *p, BIGNUM *value);

guchar 	 buffer_pop_byte		(buffer *p);
guint32	 buffer_pop_uint32		(buffer *p);
guchar 	*buffer_pop_string		(buffer *p, gint *length);
buffer 	*buffer_pop_string_as_buffer	(buffer *p);
guchar 	*buffer_pop_string_as_cstring	(buffer *p);
BIGNUM 	*buffer_pop_bignum		(buffer *p);

buffer	*buffer_hash			(buffer *p);
void 	 buffer_truncate 		(buffer *b, gint length);
void	 buffer_seek 			(buffer *b, guint index);

buffer	*buffer_concat			(const buffer *a, const buffer *b);
buffer	*buffer_copy 			(const buffer *b);

#endif
