/* part of libssh
 * (c) Copyright 2001 Ian McKellar <yakk@yakk.net>
 */

#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>

#include "utils.h"
#include "buffer.h"

void
debug_bytes (gchar *prefix, gchar *data, gint length)
{
	int i, j;
	int rowlen = 16;

	/* return; */

	for (i=0; i<length; i+=rowlen) {
		g_print ("%12.12s ", prefix);
		for (j=0; j<rowlen && (i+j)<length; j++) {
			g_print ("%02X ", (guchar)data[i+j]);
		}

		for (; j<rowlen; j++) {
			g_print ("   ");
		}

		for (j=0; j<rowlen && (i+j)<length; j++) {
			if (isprint (data[i+j])) {
				g_print ("%c", (guchar)data[i+j]);
			} else {
				g_print (".");
			}
		}

		g_print ("\n");
	}
}

void
debug_buffer (gchar *prefix, buffer *buffer)
{
	debug_bytes (prefix, buffer->payload, buffer->payload_length);
}

void
randomize_bytes (gchar *buffer, gint length)
{
	static int random_fd = -1;

	if (random_fd == -1) {
		random_fd = open ("/dev/random", O_RDONLY);
	}

	read (random_fd, buffer, length);
}
